/**
 *
 *  @author Panoszewski Mateusz S14243
 *
 */

package zad2;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class Anagrams{

	List<String> words;
	
	public Anagrams(String fname) {
		words = new ArrayList<String>();
		try {
			BufferedReader inputStream = new BufferedReader(new FileReader(fname));
			String line;
			while ((line = inputStream.readLine()) != null ){
				String[] values = line.split("\\s+");
				for(String s: values)					
				words.add(s);
			}
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public List<List<String>> getSortedByAnQty() {
		Collection<List<String>> anagrams = words.stream().collect(Collectors.groupingBy(w -> {
            char[] chars = w.toCharArray();
            Arrays.sort(chars);
            return new String(chars);
        })).values();
		List< List<String>> list = new ArrayList< List<String> >( anagrams );
		Collections.sort(list, new Comparator<List<String>>() {

			@Override
			public int compare(List<String> arg0, List<String> arg1) {
				// TODO Auto-generated method stub
				return arg0.get(0).compareTo(arg1.get(0));
			}
			
		});
		Collections.sort(list, new Comparator<List<String>>() {

			@Override
			public int compare(List<String> arg0, List<String> arg1) {
				// TODO Auto-generated method stub
				return arg1.size() - arg0.size();
			}
			
		});
		return list;
		
	}

	public String getAnagramsFor(String next) {
		List<List<String>> anagramsList = getSortedByAnQty();
		List<String> result = null;
		String resultText = next + ": ";
		for(List<String> list : anagramsList) {
			if(list.contains(next)) {
				result = list;
				result.remove(next);
			}
		}
		resultText += result;
		
		return resultText;
	}

}  
