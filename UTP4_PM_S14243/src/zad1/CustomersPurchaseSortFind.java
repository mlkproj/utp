/**
 *
 *  @author Panoszewski Mateusz S14243
 *
 */

package zad1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CustomersPurchaseSortFind {
	
	private List<Purchase> ordersList = new ArrayList<Purchase>();

	public void readFile(String fname) {
		try {
			BufferedReader inputStream = new BufferedReader(new FileReader(fname));
			String line;
			while ((line = inputStream.readLine()) != null ){
				String[] values = line.split(";");
				Purchase order = new Purchase(values[0], values[1], values[2], Double.parseDouble(values[3]), Double.parseDouble(values[4]));						
				ordersList.add(order);
			}
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public void showSortedBy(String type) {
		System.out.println(type);
		switch (type) {
		  case "Nazwiska":
			  Collections.sort(ordersList, new Comparator<Purchase>() {
			        @Override
			        public int compare(Purchase a, Purchase b){
			            return a.getId().compareTo(b.getId());
			        }
			    	});
		    	Collections.sort(ordersList, new Comparator<Purchase>() {
		        @Override
		        public int compare(Purchase a, Purchase b){
		            return a.getName().compareTo(b.getName());
		        }
		    	});		
		    	for (Purchase purchase : ordersList) {
		    		System.out.println(purchase.getId() + ";" + purchase.getName() + ";" + purchase.getProduct() + ";" + purchase.getPrice() + ";" + purchase.getAmount());
		    	}
		    	System.out.println("");
		    	break;

		  case "Koszty":
		    	Collections.sort(ordersList, new Comparator<Purchase>() {
		    		public int compare(Purchase a, Purchase b){
			            return b.getTotalPrice() == a.getTotalPrice() ? 
			            		a.getId().compareTo(b.getId())
			            		: Double.compare(b.getTotalPrice(), a.getTotalPrice());
			        }
			    });	
				for (Purchase purchase : ordersList) {
					System.out.println(purchase.getId() + ";" + purchase.getName() + ";" + purchase.getProduct() + ";" + purchase.getPrice() + ";" + purchase.getAmount() + " (koszt: " + purchase.getTotalPrice() + ")");
				}
				System.out.println("");
				break;

		  default:
			  System.out.println("Błędny wybór sortowania");
			  break;
		}
		
		
	}

	public void showPurchaseFor(String id) {
		System.out.println("Klient " + id);
		Collections.sort(ordersList, new Comparator<Purchase>() {
	        @Override
	        public int compare(Purchase a, Purchase b){
	            return b.getProduct().compareTo(a.getProduct());
	        }
	    	});
		for (Purchase purchase : ordersList) {
			if(purchase.getId().equals(id))
			System.out.println(purchase.getId() + ";" + purchase.getName() + ";" + purchase.getProduct() + ";" + purchase.getPrice() + ";" + purchase.getAmount());
		}
		System.out.println("");
	}
}
