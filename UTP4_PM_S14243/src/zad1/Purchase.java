/**
 *
 *  @author Panoszewski Mateusz S14243
 *
 */

package zad1;


public class Purchase {
	
	private String id, name, product; 
	private double price, amount, totalPrice;
	
	public Purchase(String id, String name, String product, double price, double amount) {
		this.setId(id);
		this.setName(name);
		this.setProduct(product);
		this.setPrice(price);
		this.setAmount(amount);
		this.setTotalPrice(amount * price);
		
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}


}
