package zad2;

import java.io.*;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toMap;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;


public class ProgLang<K,V> {

	File file;
	
	public ProgLang(String filePath) throws IOException {
		file = new File(filePath); 
	}

	public Map<K, Collection<V>> getLangsMap() throws IOException {
		Map<K, Collection<V>> langsMap = new LinkedHashMap<K, Collection<V>>();
		String line;
		FileReader fr = new FileReader(file);
	    BufferedReader reader = new BufferedReader(fr);
	    while ((line = reader.readLine()) != null)
	    {
	        String[] parts = line.split("\\t");
	        if (parts.length >= 2)
	        {
	            K key = (K) parts[0];
	            List<V> value = new ArrayList<>();
	            for(int i = 1; i < parts.length; i++)
	            	value.add((V)parts[i]);
	            LinkedHashSet<V> set = new LinkedHashSet<V>(value);
	            langsMap.put(key, set);
	            
	        } else {
	            System.out.println("ignoring line: " + line);
	        }
	    }
	    return langsMap;
	}
	
	public Map<K, Collection<V>> getProgsMap() throws IOException{
		Map<K, Collection<V>> result = new LinkedHashMap<K, Collection<V>>();
		LinkedHashSet<K> progs = new LinkedHashSet<K>();
		LinkedHashSet<V> langs = new LinkedHashSet<V>();
		Map<K, Collection<V>> langsMap = getLangsMap();
		langsMap.values().forEach(e -> progs.addAll((Collection<? extends K>) e));
		progs.forEach(e -> result.put(e, new LinkedHashSet<V>()));
		progs.forEach(e -> {
			langsMap
			.keySet()
			.forEach(n -> {
					if(langsMap
							.get(n)
							.contains(e)){
								result.get(e).add((V) n);
											}});;
					;});;
		return result;
	}
	
	public Map<K, Collection<V>> getProgsMapForNumOfLangsGreaterThan(int n) throws IOException{
		return filtered(getProgsMap(), (p) -> p.size() > 1);	
	}
	
	public <U> Map<K, U> filtered(Map<K, U> map,  Predicate<U> predicate){
		LinkedHashMap<K, U> result = new LinkedHashMap<K, U>();
	    for (K element: map.keySet()) {
	        if (predicate.test(map.get(element))) {
	            result.put(element, map.get(element));
	        }
	    }
	    return result;
	}

	public Map<K, Collection<V>> getProgsMapSortedByNumOfLangs() throws IOException {

		Comparator<Map.Entry<K, Collection<V>>> comp = (a, b) -> (b.getValue().size()==a.getValue().size()?((String)a.getKey()).compareTo((String)b.getKey()):(b.getValue().size() - a.getValue().size()));

		return sorted(getProgsMap(), comp);
	}
	
	
	public <U> Map<K, U> sorted(Map<K, U> map,  Comparator<Map.Entry<K, U>> comp){
		List<Map.Entry<K, U>> list = new LinkedList<Map.Entry<K, U>>(map.entrySet());
		Collections.sort(list, comp);
		LinkedHashMap<K, U> sortedMap = new LinkedHashMap<K, U>();
        
		for (Map.Entry<K, U> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
		return sortedMap; 
	}

	public Map<K, Collection<V>> getLangsMapSortedByNumOfProgs() throws IOException {
		
		Comparator<Map.Entry<K, Collection<V>>> comp = (a, b) -> ((b.getValue().size() - a.getValue().size()));
		
		return sorted(getLangsMap(), comp);
	}

}
