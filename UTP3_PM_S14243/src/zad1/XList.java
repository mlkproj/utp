package zad1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class XList<T> extends ArrayList<T>{


	public static XList<String> charsOf(String text) {
		List<String> filtered = Stream
	            .of(text)
	            .map(w -> w.split("")).flatMap(Arrays::stream)
	            .collect(Collectors.toList());
		return new XList<String>(filtered);
	}

	public static XList<String> tokensOf(String s) {
		List<String> filtered = Stream
	            .of(s)
	            .map(w -> w.split("\\s+")).flatMap(Arrays::stream)
	            .collect(Collectors.toList());
	               
	        return new XList<String>(filtered);
	}
	
	public static XList<String> tokensOf(String s, String sep) {
		
		List<String> filtered = Stream
	            .of(s)
	            .map(w -> w.split(sep)).flatMap(Arrays::stream)
	            .collect(Collectors.toList());
	        return new XList<String>(filtered);
	}

	public XList() {
		super();
	}

	public XList(Collection<T> arg0) {
		super(arg0);
	}
	
	@SafeVarargs
	public XList(T... args) {
		for (T elt : args) this.add(elt);
	}

	public XList(int arg0) {
		super(arg0);
	}

	@SafeVarargs
	public static <T> XList<T> of(T... args) {
		return new XList<T>(args); 
	}
	
	public static <T> XList<T> of(Collection<T> arg0) {
		// TODO Auto-generated method stub
		return  new XList<T>(arg0);
	}

	public XList<T> union(Collection<T> list2) {
		XList<T> n = new XList<T>(); 
		this.forEach(c -> n.add(c));
		list2.forEach(c -> n.add(c));
		return n;
	}
	
	public XList<T> union(T... args) {
		XList<T> l = new XList<T>(this); 
		XList<T> r = new XList<T>(args);
		r.forEach(c -> l.add(c));
		return l;
	}

	public XList<T> diff(Collection<T> set) {
		XList<T> n = new XList<T>();
		this.forEach(c -> {
			if(!set.contains(c)) {
				n.add(c);
			}
		});
		return n;
	}

	public XList<T> unique() {
		List<T> n = stream()
		           .distinct()
		           .collect(Collectors.toList());
		return new XList<T>(n);
	}

	XList<T> trimFrom(int from, XList<T> list){
		XList<T> result = new XList<T>(this.subList(from, this.size()));
		return result;
	}
	
	public XList<XList<T>> combine() {
		XList<XList<T>> lists = new XList<>();
		
		if(this.isEmpty()) {
		    	lists.add(new XList<T>());
		    	return new XList<XList<T>>(lists);
		    }
		else {
			if((this.get(0)) instanceof List) {
			List<T> sublist = (List<T>) this.get(0);
			XList<XList<T>> rest = trimFrom(1, (XList<T>)this).combine();
			
				for(XList<T> x : rest) {
					for(T t : sublist ) {
						XList<T> result = new XList<T>();
						result.add(t);
						result.addAll(x);
					
						lists.add(result);
					}
			
				}
			}
		}
		return lists;
	}

	public <U> XList<U> collect(Function<T, U> func) {

		return new XList<U> (this.stream().map(func).collect(Collectors.toList()));
	}

	public String join() {

		return this.stream().map(e -> e.toString()).collect(Collectors.joining(""));
	}
	
	public String join(String sep) {

		return this.stream().map(e -> e.toString()).collect(Collectors.joining(sep));
	}

	public <U> void forEachWithIndex(BiConsumer<T,Integer> func) {
		for(int i = 0; i < this.size(); i++) {
			func.accept(this.get(i), i);
		}
	}
	
}





	

